
package com.cramanager.application.project;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectService {

	@Autowired
	ProjectRepository projectsRepository;

	/**
	 * Constructor 
	 */
	public ProjectService() {
	}

	/**
	 * Method for creating a project
	 * @param project
	 * @return true if the project has been created, false if it failed
	 */
	public boolean createProject(Project project) {
		Objects.requireNonNull(project);
		return projectsRepository.save(project) != null;
	}

	/**
	 * Method for getting all projects
	 * @return List<Project>
	 */
	public List<Project> getAllProjects() {
		return projectsRepository.findAll();
	}

	/**
	 * Method for getting the project by id
	 * @param id
	 * @return Optional<Project>
	 */
	public Optional<Project> findProjectById(long id) {
		return projectsRepository.findProjectById(id);
	}
	
	/**
	 * Method for getting the project by name
	 * @param name
	 * @return Optional<Project>
	 */
	public Optional<Project> findProjectByName(String name) {
		Objects.requireNonNull(name);
		return projectsRepository.findProjectByName(name);
	}

	/**
	 * Method for deleting the project by id in database
	 * @param id
	 */
	public void deleteProject(Long id) {
		Project project = projectsRepository.findOne(id);
		projectsRepository.delete(project);
	}
}
