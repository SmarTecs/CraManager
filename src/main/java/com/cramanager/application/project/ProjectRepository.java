
package com.cramanager.application.project;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProjectRepository extends PagingAndSortingRepository <Project, Long>{
	/**
	 * Method for getting all Project
	 */
	public List<Project> findAll();
	
	/**
	 * Method for saving/creating a new Project in Database
	 */
	public <S extends Project> S save(S project);
	
	/**
	 * Method for getting a specific project by name
	 * @param name
	 * @return Optional<Project> containing the correct project
	 */
	public Optional<Project> findProjectByName(String name);
	
	/**
	 * Method for getting a specific project by id
	 * @param id
	 * @return Optional<Project> containing the correct project
	 */
	public Optional<Project> findProjectById(Long id);
	
	/**
	 * Method for deleting a specific project by id in database
	 */
	public void delete(Project project);
	
}
