package com.cramanager.application.project;

import java.util.Objects;

public class ProjectChecking {
	
	/**
	 * Method gathering all checking
	 * @param project
	 * @return boolean
	 */
	public static boolean checkProject(Project project) {
		Objects.requireNonNull(project);
		if(!checkProjectClient_Id(project)) {
			return false;
		}
		if(!checkProjectUser_Id(project)) {
			return false;
		}
		if(!checkProjectCorrectDate(project)) {
			return false;
		}
		return true;
	}
	
	/**
	 * the project user_id must be positiv
	 * @param project
	 * @return boolean
	 */
	public static boolean checkProjectUser_Id(Project project) {
		return project.getUser_id() > 0 ? true : false;
	}
	
	/**
	 * the project client_id must be positiv
	 * @param project
	 * @return boolean
	 */
	public static boolean checkProjectClient_Id(Project project) {
		return project.getClient_id() > 0 ? true : false;
	}
	
	/**
	 * the startDate must be before the endDate
	 * @param project
	 * @return boolean
	 */
	public static boolean checkProjectCorrectDate(Project project) {
		return project.getStartDate().before(project.getEndDate()) ? true : false;
	}
}
