
package com.cramanager.application.project;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectController {

	@Autowired
	private ProjectService projectService;

	/**
	 * Request POST for creating a project with JSON body
	 * @param project
	 * @return ResponseEntity with Status OK or BAD_REQUEST
	 */
	@RequestMapping(value = "/projects", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<String> createProject(@RequestBody Project project) {
		Objects.requireNonNull(project);
		if(!ProjectChecking.checkProject(project)) {
			return new ResponseEntity<>("check problem",HttpStatus.BAD_REQUEST);
		}
		if (projectService.createProject(project)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

	/**
	 * Request GET for getting all projects in database
	 * @return List of all Projects
	 */
	@RequestMapping(value = "/projects", method = RequestMethod.GET)
	public List<Project> getProjects() {
		List<Project> projects = projectService.getAllProjects();
		System.err.println("RECU GET ALL");
		return projects;
	}

	/**
	 * Request GET for getting {id} project
	 * @param id
	 * @return ResponseEntity with Status OK or BAD_REQUEST containing the project {id}
	 */
	@RequestMapping(value = "/projects/{id}", method = RequestMethod.GET)
	public ResponseEntity<Project> getProjectById(@PathVariable Long id) {
		return new ResponseEntity<>(projectService.findProjectById(id).get(), HttpStatus.OK);
	}
	
	/**
	 * Request DELETE for deleting {id} project
	 * @param id
	 * @return ResponseEntity with Status OK or BAD_REQUEST containing the project {id}
	 */
	@RequestMapping(value = "/projects/{id}", method = RequestMethod.DELETE)
	public void deleteProjectById(@PathVariable Long id) {
		projectService.deleteProject(id);
	}

}
