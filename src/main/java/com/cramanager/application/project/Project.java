
package com.cramanager.application.project;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="project")
public class Project implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name= "code")
	private String code;
	
	@Column(name= "user_id")
	private Integer user_id;
	
	@Column(name= "client_id")
	private Integer client_id;
	
	@Column(name= "name")
	private String name;

	@Column(name= "startDate")
	private Date startDate;

	@Column(name= "endDate")
	private Date endDate;

	@Column(name= "showInTeam")
	private boolean showInTeam;

	@Column(name= "canceled")
	private boolean canceled;
	
	@Column(name= "comments")
	private String comments;
	
	@Column(name= "accountingEndDate")
	private Date accountingEndDate;
	
	@Column(name= "tags")
	private String tags;
	
	/**
	 * Constructor without parameters
	 */
	public Project() {
	}

	/**
	 * Constructor with parameters
	 * @param code
	 * @param userLogin
	 * @param clientCode
	 * @param name
	 * @param start
	 * @param end
	 * @param showInTeam
	 * @param comments
	 * @param accountingEndDate
	 * @param tags
	 */
	public Project(String code, Integer userLogin, Integer clientCode, String name, Date start, Date end,
			boolean showInTeam, String comments, Date accountingEndDate, String tags) {
		this.code = Objects.requireNonNull(code);
		this.user_id = Objects.requireNonNull(userLogin);
		this.client_id = Objects.requireNonNull(clientCode);
		this.name = Objects.requireNonNull(name);
		this.startDate = Objects.requireNonNull(start);
		this.endDate = Objects.requireNonNull(end);
		this.showInTeam = showInTeam;
		this.canceled = false;
		this.comments = Objects.requireNonNull(comments);
		this.accountingEndDate = Objects.requireNonNull(accountingEndDate);
		this.tags = Objects.requireNonNull(tags);
	}

	/**
	 * Getter code
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Setter code
	 * @param code
	 */
	public void setCode(String code) {
		this.code = Objects.requireNonNull(code);
	}

	/**
	 * Getter id
	 * @return id
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Getter user_id
	 * @return user_id
	 */
	public int getUser_id() {
		return user_id;
	}

	/**
	 * Setter user_id
	 * @param user_id
	 */
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	/**
	 * Getter client_id
	 * @return client_id
	 */
	public int getClient_id() {
		return client_id;
	}

	/**
	 * Setter client_id
	 * @param client_id
	 */
	public void setClient_id(Integer client_id) {
		this.client_id = client_id;
	}

	/**
	 * Getter Name
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter Name
	 * @param name
	 */
	public void setName(String name) {
		this.name = Objects.requireNonNull(name);
	}

	/**
	 * Getter StartDate
	 * @return startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Setter StartDate
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		this.startDate = Objects.requireNonNull(startDate);
	}

	/**
	 * Getter EndDate
	 * @return endDate
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Setter EndDate
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = Objects.requireNonNull(endDate);
	}

	/**
	 * Getter ShowInTeam
	 * @return showInTeam
	 */
	public boolean isShowInTeam() {
		return showInTeam;
	}

	/**
	 * Setter ShowInTeam
	 * @param showInTeam
	 */
	public void setShowInTeam(boolean showInTeam) {
		this.showInTeam = Objects.requireNonNull(showInTeam);
	}

	/**
	 * Getter canceled
	 * @return canceled
	 */
	public boolean isCanceled() {
		return canceled;
	}

	/**
	 * Setter canceled
	 * @param canceled
	 */
	public void setCanceled(boolean canceled) {
		this.canceled = Objects.requireNonNull(canceled);
	}

	/**
	 * Getter comments
	 * @return comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Setter Comments
	 * @param comments
	 */
	public void setComments(String comments) {
		this.comments = Objects.requireNonNull(comments);
	}

	/**
	 * Getter AccountingEndDate
	 * @return accountingEndDate
	 */
	public Date getAccountingEndDate() {
		return accountingEndDate;
	}

	/**
	 * Setter AccountingEndDate
	 * @param accountingEndDate
	 */
	public void setAccountingEndDate(Date accountingEndDate) {
		this.accountingEndDate = Objects.requireNonNull(accountingEndDate);
	}

	/**
	 * Getter Tags
	 * @return tags
	 */
	public String getTags() {
		return tags;
	}

	/**
	 * Setter Tags
	 * @param tags
	 */
	public void setTags(String tags) {
		this.tags = Objects.requireNonNull(tags);
	}

}