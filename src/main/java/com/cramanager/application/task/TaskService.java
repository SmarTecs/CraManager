
package com.cramanager.application.task;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Class responsible for checking application tasks' rules
 * @author jason
 *
 */
@Component
public class TaskService {

	@Autowired
	private TaskRepository taskRepository;

	/**
	 * Retrieves all tasks
	 * @return an <code>Iterable</code> of all tasks
	 */
	public Iterable<Task> getAllTasks() {
		return taskRepository.findAll();
	}

	/**
	 * Retrieves a task
	 * @param idTask task's id
	 * @return the task with the specified id
	 */
	public Optional<Task> findTaskById(int idTask) {
		return Optional.ofNullable(taskRepository.findOne(Integer.valueOf(idTask)));
	}

	/**
	 * Creates a task
	 * @param task the task to create
	 * @return the task created
	 */
	public Task createTask(Task task) {
		return taskRepository.save(task);
	}
}
