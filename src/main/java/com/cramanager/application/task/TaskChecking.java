package com.cramanager.application.task;

/**
 * Class responsible of checking revolving around tasks
 * @author jason
 *
 */
public class TaskChecking {
	/**
	 * Checks if task data are coherent
	 * @param task the Task to check
	 * @return <code>false</code> if end of task is before start, <code>true</code> otherwise
	 */
	public static boolean check(Task task) {
		return !task.isEndBeforeStart();
	}
}
