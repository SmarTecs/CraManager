
package com.cramanager.application.task;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository making the link between the application and the database for all task-related operations
 * @author jason
 *
 */
@Repository
public interface TaskRepository extends PagingAndSortingRepository<Task, Integer> {
	/**
	 * Retrieves a task
	 * @param idTask task's id
	 * @return the task with the specified id
	 */
	Task findTaskById(@Param("id") int idTask);
}
