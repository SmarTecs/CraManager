
package com.cramanager.application.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for task-related queries
 * @author jason
 *
 */
@RestController
public class TaskController {

	@Autowired
	private TaskService taskService;

	/**
	 * Retrieves all tasks
	 * @return an <code>Iterable</code> of all tasks
	 */
	@RequestMapping(value = "/tasks", method = RequestMethod.GET)
	public Iterable<Task> getTasks() {
		return taskService.getAllTasks();
	}

	/**
	 * Retrieves a task
	 * @param id task's id
	 * @return the task with the specified id
	 */
	@RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
	public ResponseEntity<Task> findTaskById(@PathVariable int id) {
		return new ResponseEntity<>(taskService.findTaskById(id).orElse(null), HttpStatus.OK);
	}

	/**
	 * Creates a task
	 * @param task task to create
	 * @return the task created
	 */
	@RequestMapping(value = "/tasks", method = RequestMethod.POST, produces = "application/json", consumes ="application/json")
	public ResponseEntity<Task> createTask(@RequestBody Task task) {
		if (!TaskChecking.check(task)) {
			return new ResponseEntity<>(null, HttpStatus.OK);
		}
		return new ResponseEntity<>(taskService.createTask(task), HttpStatus.OK);
	}
}
