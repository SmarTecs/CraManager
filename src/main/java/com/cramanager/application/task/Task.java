
package com.cramanager.application.task;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Task definition class
 * @author jason
 *
 */
@Entity
public class Task implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="code", nullable=false, length=32)
	private String code;
	
	@Column(name="project_id", nullable=false, length=11)
	private int projectId;
	
	@Column(name="name", nullable=false, length=32)
	private String name;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name="startdate", nullable=false)
	private Date startDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@Column(name="enddate", nullable=true)
	private Date endDate;
	
	@Column(name="activated", nullable=false)
	private boolean activated;
	
	/**
	 * Creates an empty task.
	 * Used by TaskController to parse the Json from HTTP queries
	 * @see TaskController
	 */
	public Task() {}
	
	/**
	 * Creates a task
	 * @param id task's id
	 * @param code task's code
	 * @param projectId id of the project in which this task is
	 * @param name task's name
	 * @param startDate start of the task
	 * @param endDate end of the task
	 * @param activated if the task exists or not
	 */
	public Task(int id, String code, int projectId, String name, Date startDate, Date endDate,
			boolean activated) {
		super();
		if (endDate.before(startDate)) {
			throw new IllegalArgumentException("Start should be before end");
		}
		this.id = id;
		this.code = code;
		this.projectId = projectId;
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
		this.activated = activated;
	}

	/**
	 * 
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getCode() {
		return code;
	}

	/**
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 
	 * @return
	 */
	public int getProjectId() {
		return projectId;
	}

	/**
	 * 
	 * @param projectId
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * Set task's start<br>
	 * If the new start is after current end, then it will not update
	 * @param start
	 */
	public void setStart(Date start) {
		if (start.after(endDate)) {
			throw new IllegalArgumentException("Start should be before end");
		}
		this.startDate = start;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * Set task's end<br>
	 * If the new end is before current start, it will not update
	 * @param end
	 */
	public void setEnd(Date end) {
		if (end.before(startDate)) {
			throw new IllegalArgumentException("Start should be before end");
		}
		this.endDate = end;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getActivated() {
		return activated;
	}

	/**
	 * 
	 * @param activated
	 */
	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	
	/**
	 * Checks if end is before start
	 * @return true if current end is before current start
	 */
	public boolean isEndBeforeStart() {
		return endDate.before(startDate);
	}

	/**
	 * 
	 * @return
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
