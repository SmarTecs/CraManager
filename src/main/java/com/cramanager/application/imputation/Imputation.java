
package com.cramanager.application.imputation;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Imputation definition class
 * @author Gwenael
 */
@Entity
@Table(name="imputation")
public class Imputation implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idImputation;
	
	@Column(name= "contributor_id")
	private int idContributor;
	
	@Column(name= "task_id")
	private int idTask;
	
	@Column(name= "date_imputation")
	private Date dateImputation;
	
	@Column(name= "duration")
	private int duration;
	
	@Column(name= "activated")
	private boolean activated;

	/**
	 * Creates an empty imputation
	 * Used by ImputationController to parse the Json from HTTP queries
	 * @see ImputationController
	 */
	public Imputation() {}
	
	/**
	 * Creates an Imputation
	 * @param idContributor = id of the associated Contributor
	 * @param idTask = id of the associated Task
	 * @param dateImputation = creation date of the Imputation
	 * @param duration = number of work granularities of the associated Contributor on the associated Task the creation date of the Imputation 
	 * @param activated = if the Imputation is activated or not
	 * @throws NullPointerException if idContributor, idTask, dateImputation, duration or activated are null
	 */
	public Imputation(int idContributor, int idTask, Date dateImputation, int duration, boolean activated) {
		super();
		Objects.requireNonNull(idContributor);
		Objects.requireNonNull(idTask);
		Objects.requireNonNull(dateImputation);
		Objects.requireNonNull(duration);
		Objects.requireNonNull(activated);
		this.idContributor = idContributor;
		this.idTask = idTask;
		this.dateImputation = dateImputation;
		this.duration = duration;
		this.activated = activated;
	}
	
	/**
	 * get id of the Imputation
	 * @return id of the Imputation
	 */
	public long getIdImputation() {
		return idImputation;
	}
	
	/**
	 * get id of the associated Contributor
	 * @return id of the associated Contributor
	 */	
	public int getIdContributor() {
		return idContributor;
	}
	
	/**
	 * set id of the associated Contributor
	 * @param idContributor = id of the associated Contributor
	 */
	public void setIdContributor(int idContributor) {
		this.idContributor = idContributor;
	}
	
	/**
	 * get id of the associated Task
	 * @return id of the associated Task
	 */
	public int getIdTask() {
		return idTask;
	}
	
	/**
	 * set id of the associated Task
	 * @param idTask = id of the associated Task
	 */
	public void setIdTask(int idTask) {
		this.idTask = idTask;
	}
	
	/**
	 * get creation date of the Imputation
	 * @return creation date of the Imputation
	 */
	public Date getDateImputation() {
		return dateImputation;
	}
	
	/**
	 * set creation date of the Imputation
	 * @param dateImputation = creation date of the Imputation
	 */
	public void setDateImputation(Date dateImputation) {
		this.dateImputation = dateImputation;
	}

	/**
	 * get number of work granularities of the associated Contributor on the associated Task the creation date of the Imputation 
	 * @return number of work granularities of the associated Contributor on the associated Task the creation date of the Imputation 
	 */
	public int getDuration() {
		return duration;
	}
	
	/**
	 * set number of work granularities of the associated Contributor on the associated Task the creation date of the Imputation
	 * @param duration = number of work granularities of the associated Contributor on the associated Task the creation date of the Imputation
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	/**
	 * get activation status of the Imputation
	 * @return activation status of the Imputation
	 */
	public boolean getActivated() {
		return activated;
	}
	
	/**
	 * set activation status of the Imputation
	 * @param activated = activation status of the Imputation
	 */
	public void setActivated(boolean activated) {
		this.activated = activated;
	}
	
	/**
	 * Update all fields excluding idImputation of actual Imputation with fields of updated Imputation
	 * @param impUpdated = updated Imputation
	 */
	public void update(Imputation impUpdated) {
		this.setIdTask(impUpdated.getIdTask());
		this.setIdContributor(impUpdated.getIdContributor());
		this.setDateImputation(impUpdated.getDateImputation());
		this.setDuration(impUpdated.getDuration());
		this.setActivated(impUpdated.getActivated());
	}

}