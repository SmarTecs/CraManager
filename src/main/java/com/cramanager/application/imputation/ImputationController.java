
package com.cramanager.application.imputation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Imputation controller interface
 * @author Gwenael
 */
@RestController
public class ImputationController {

	@Autowired
	private ImputationService imputationService;

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/imputations", method = RequestMethod.GET)
	public List<Imputation> getImputations() {
		List<Imputation> imps = imputationService.getImputations();
		return imps;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/imputations/{id}", method = RequestMethod.GET)
	public List<Imputation> getImputationByIdTask(@PathVariable int id) {
		List<Imputation> imp = imputationService.getImputationsByIdTask(id);
		return imp;
	}
	
	/**
	 * 
	 * @param id
	 * @param imputationUpdated
	 * @return
	 */
	@RequestMapping(value = "/imputations/{id}", method = RequestMethod.PATCH)
	public ResponseEntity<String> updateImputation(@PathVariable int id, @RequestBody Imputation imputationUpdated) {
		try{
			imputationService.updateImputation(id, imputationUpdated);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (IllegalArgumentException iae) {
			return new ResponseEntity<>(iae.getMessage(), HttpStatus.BAD_REQUEST);
		} catch (NullPointerException npe) {
			return new ResponseEntity<>("imputation id cannot be founded in database", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 
	 * @param imputation
	 * @return
	 */
	@RequestMapping(value = "/imputations", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createImputation(@RequestBody Imputation imputation) {
		try{
			imputationService.createImputation(imputation);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (IllegalArgumentException iae) {
			return new ResponseEntity<>(iae.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 
	 * @param id
	 */
	@RequestMapping(value = "/imputations/{id}", method = RequestMethod.DELETE)
	public void deleteImputation(@PathVariable int id) {
		imputationService.deleteImputation(id);
	}

}
