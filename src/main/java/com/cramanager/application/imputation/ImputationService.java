
package com.cramanager.application.imputation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Imputation service class
 * @author Gwenael
 */
@Component
public class ImputationService {

	@Autowired
	private ImputationRepository imputationRepository;
	
	@Autowired
	private ImputationChecking imputationChecking;
	
	/**
	 * Get all instances of imputation
	 * @return all imputations
	 */
	public List<Imputation> getImputations() {
		return imputationRepository.findAll();
	}
	
	/**
	 * Get all instances of imputation with the associated task id
	 * @param idTask = associated task id
	 * @return all imputations for the given task id
	 */
	public List<Imputation> getImputationsByIdTask(int idTask) {
		return imputationRepository.findByIdTask(idTask);
	}
	
	/**
	 * Create a new instance of imputation
	 * @param imputation = instance of imputation containing all filled fields
	 * @return given instance of imputation
	 */
	public Imputation createImputation(Imputation imputation) {
		imputationChecking.checkImputation(imputation);
		return imputationRepository.save(imputation);
	}
	
	/**
	 * Delete an instance of imputation with the given imputation id
	 * @param idImputation = imputation id of the imputation to delete
	 */
	public void deleteImputation(long idImputation) {
		Imputation imp = imputationRepository.findOne(idImputation);
		if(imp == null)
			throw new NullPointerException();
		imputationRepository.delete(imp);
	}

	/**
	 * Update the instance of Imputation with the given imputation id
	 * @param idImputation = imputation id of the imputation to update
	 * @param imputationUpdated = instance of imputation containing all updated fields
	 * @return updated instance of imputation
	 */
	public Imputation updateImputation(int idImputation, Imputation imputationUpdated) {
		imputationChecking.checkImputation(imputationUpdated);
		
		Imputation imp = imputationRepository.findOne(idImputation);
		if(imp == null)
			throw new NullPointerException();
		
		imp.update(imputationUpdated);
		
		return imputationRepository.save(imp);
	}

}
