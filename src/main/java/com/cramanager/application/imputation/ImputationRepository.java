
package com.cramanager.application.imputation;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Imputation repository interface
 * @author Gwenael
 */
public interface ImputationRepository extends PagingAndSortingRepository<Imputation, Long>{
	
	/**
	 * Returns all instances of Imputation.
	 * @return all imputations
	 */
	public List<Imputation> findAll();
	
	/**
	 * Saves a given imputation. Use the returned instance for further operations as the save operation might have changed the imputation instance completely.
	 * @param imputation - must not be null
	 * @return the saved imputation will never be null
	 */
	public <S extends Imputation> S save(S imputation);
	
	/**
	 * Retrieves an imputation by its id.
	 * @param idImputation = id of the imputation
	 * @return the imputation with the given id, null if not present
	 */
	public Imputation findOne(long idImputation);
	
	/**
	 * Deletes a given imputation
	 * @param imputation = imputation to delete
	 * @throws IllegalArgumentException in case the given entity is null
	 */
	void delete(Imputation imputation);
	
	/**
	 * Retrieves all instances of imputation by their task id.
	 * @param idTask = task id of the imputations
	 * @return all imputations with the given task id, null if not present
	 */
	public List<Imputation> findByIdTask(int idTask);
}

