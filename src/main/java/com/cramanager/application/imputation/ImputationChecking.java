package com.cramanager.application.imputation;

import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cramanager.application.contributor.Contributor;
import com.cramanager.application.task.Task;
import com.cramanager.application.task.TaskRepository;

/**
 * Imputation checking class
 * @author Gwenael
 */
@Component
public class ImputationChecking{
	
	@Autowired
	private TaskRepository taskRepository;
	
	/**
	 * Check validity of an imputation
	 * This function tests task id, contributor id, creation date of imputation, duration of imputation and activation status of imputation
	 * @param imputation = imputation to check
	 * @return imputation past in argument
	 * @throws IllegalArgumentException if imputation doesn't pass tests; message of this exception is a JSON string containing a OK or error message for each tested attribut
	 */
	public Imputation checkImputation(Imputation imputation) {
		StringBuilder jsonError = new StringBuilder("{");
		boolean errorFounded = false;
		Contributor contributor = null;
		Task task = null;
		
		try {
			contributor = checkImputationIdContributor(imputation.getIdContributor());
			jsonError.append("\"idContributor\": ").append("\"OK\", ");
		} catch (IllegalArgumentException iae) {
			errorFounded = true;
			jsonError.append("\"idContributor\": \"").append(iae.getMessage()).append("\", ");
		}
		
		try	{	
			task = checkImputationIdTask(imputation.getIdTask());
			jsonError.append("\"idTask\": ").append("\"OK\", ");
		} catch (IllegalArgumentException iae) {
			errorFounded = true;
			jsonError.append("\"idTask\": \"").append(iae.getMessage()).append("\", ");
		}
		
		try {
			checkImputationDateImputation(imputation.getDateImputation(), task.getStartDate(), task.getEndDate(),/*contributor.getDesactivateDate()*/null);
			jsonError.append("\"dateImputation\": ").append("\"OK\", ");
		} catch (IllegalArgumentException iae) {
			errorFounded = true;
			jsonError.append("\"dateImputation\": \"").append(iae.getMessage()).append("\", ");
		} catch (NullPointerException npe) {
			errorFounded = true;
			jsonError.append("\"dateImputation\": \"imputation date, task starting date, task ending date or contributor desactivation date is null\", ");
		}
		
		try {	
			checkImputationDuration(imputation.getDuration());
			jsonError.append("\"duration\": ").append("\"OK\", ");
		} catch (IllegalArgumentException iae) {
			errorFounded = true;
			jsonError.append("\"duration\": \"").append(iae.getMessage()).append("\", ");
		}
		
		if(checkImputationActivated(imputation.getActivated()))
			jsonError.append("\"activated\": ").append("\"OK\" ");
		
		jsonError.append("}");
		if(errorFounded)
			throw new IllegalArgumentException(jsonError.toString());
		
		return imputation;
	}

	/**
	 * Check contributor id validity 
	 * @param idContributor = contributor id to check
	 * @return contributor associates to contributor id past in argument
	 * @throws IllegalArgumentException if contributor id is inferior to 0
	 * @throws IllegalArgumentException if contributor id cannot be founded in database
	 */
	public Contributor checkImputationIdContributor(int idContributor) {
		if(idContributor <= 0)
			throw new IllegalArgumentException("Contributor id cannot be inferior to 0");

//		Contributor contributor = contributorRepository.findOne(idContributor);
//		if(contributor == null)
//			throw new IllegalArgumentException("Contributor id cannot be founded in database");

//		return idContributor;
		return null;
	}
	
	/**
	 * Check task id validity 
	 * @param idTask = task id to check
	 * @return task associates to task id past in argument
	 * @throws IllegalArgumentException if task id is inferior to 0
	 * @throws IllegalArgumentException if task id cannot be founded in database
	 */
	public Task checkImputationIdTask(int idTask) {
		if(idTask <= 0)
			throw new IllegalArgumentException("Task id cannot be inferior to 0");
		
		Task task = taskRepository.findOne(idTask);
		if(task == null)
			throw new IllegalArgumentException("Task id cannot be founded in database");
	
		return task;
	}
	
	/**
	 * Check validity of the creation date of the imputation 
	 * @param dateImputation = creation date of imputation to check
	 * @param dateStartTask = starting date of the task
	 * @param dateEndTask = ending date of the task
	 * @param dateDesactivationDateContributor = date of desactivation of contributor
	 * @return creation date of the imputation past in argument
	 * @throws NullPointerException if one of the arguments are null
	 * @throws IllegalArgumentException if creation date of imputation is before starting date of the task
	 * @throws IllegalArgumentException if creation date of imputation is after ending date of the task
	 * @throws IllegalArgumentException if creation date of imputation is after desactivation date of the contributor
	 * @throws IllegalArgumentException if creation date of imputation is after today
	 */
	public Date checkImputationDateImputation(Date dateImputation, Date dateStartTask, Date dateEndTask, Date dateDesactivationDateContributor) {
		Objects.requireNonNull(dateImputation);
		Objects.requireNonNull(dateStartTask);
		Objects.requireNonNull(dateEndTask);
		Objects.requireNonNull(dateDesactivationDateContributor);
		
		if(dateImputation.before(dateStartTask))
			throw new IllegalArgumentException("creation date of imputation cannot be before starting date of the task");
	
		if(dateImputation.after(dateEndTask))
			throw new IllegalArgumentException("creation date of imputation cannot be after ending date of the task");
		
		if(dateImputation.after(dateDesactivationDateContributor))
			throw new IllegalArgumentException("creation date of imputation cannot be after desactivation date of the contributor");
		
		if(dateImputation.after(new Date()))
			throw new IllegalArgumentException("creation date of imputation cannnot be after today");
		
		return dateImputation;
	}
	
	/**
	 * Check duration validity 
	 * @param duration = number of work granularities of the associated Contributor on the associated Task the creation date of the Imputation
	 * @return duration past in argument
	 * @throws IllegalArgumentException if duration is inferior to 0
	 * @throws IllegalArgumentException if duration is superior to 8
	 */
	public int checkImputationDuration(int duration) {
		if(duration < 0)
			throw new IllegalArgumentException("duration cannot be inferior to 0");
		
		if(duration > 8)
			throw new IllegalArgumentException("duration cannot superior to 8");
		
		return duration;
	}
	
	/**
	 * Check activation status validity
	 * @param activated = activation status
	 * @return always true
	 */
	public boolean checkImputationActivated(boolean activated) {
		return true;
	}
	
}
