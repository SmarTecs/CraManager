
package com.cramanager.application.contributor;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Contributor repository
 * 
 * @author snadane.niveda
 *
 */
@Repository
public interface ContributorRepository extends PagingAndSortingRepository<Contributor, Integer> {

}
