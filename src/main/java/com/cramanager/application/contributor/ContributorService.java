
package com.cramanager.application.contributor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.OptionalValidatorFactoryBean;

@Component
public class ContributorService {

	@Autowired
	private ContributorRepository contributorRepository;

	/**
	 * Empty constructor
	 */
	public ContributorService() {

	}

	/**
	 * Create a contributor
	 *
	 * @param contributor
	 * @throws Exception
	 */
	public boolean createContributor(Contributor contributor) {
		Optional<Contributor> contributorWithSameId = getContributors().stream()
				.filter(c -> c.getId() == contributor.getId()).findFirst();
		if (contributorWithSameId.get() != null) {
			contributorRepository.save(contributor);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get all contributors
	 * 
	 * @return
	 */
	public List<Contributor> getContributors() {
		List<Contributor> contributors = new ArrayList<>();
		contributorRepository.findAll().forEach(c -> contributors.add(c));
		return contributors;
	}

	/**
	 * Get one specific contributor
	 * 
	 * @param id
	 * @return the contributor
	 */
	public Optional<Contributor> getOneContributor(int id) {
		return getContributors().stream().filter(c -> c.getId() == id).findFirst();
	}

}
