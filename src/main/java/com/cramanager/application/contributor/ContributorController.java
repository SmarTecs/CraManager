
package com.cramanager.application.contributor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContributorController {

	@Autowired
	private ContributorService contributorService;

	/**
	 * Create a contributor
	 * 
	 * @param contributor
	 * @return
	 */
	@RequestMapping(value = "/contributors", method = RequestMethod.POST)
	public ResponseEntity<String> createContributor(@RequestBody Contributor contributor) {
		contributorService.createContributor(contributor);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * Get all contributors
	 * 
	 * @return the list of contributors
	 */
	@RequestMapping(value = "/contributors", method = RequestMethod.GET)
	public ResponseEntity<List<Contributor>> getContributors() {
		return new ResponseEntity<>(contributorService.getContributors(), HttpStatus.OK);
	}

	/**
	 * Get one contributor
	 * 
	 * @return the specific Contributor
	 */
	@RequestMapping(value = "/contributors/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Contributor> getOneContributor(@PathVariable int id) {
		return new ResponseEntity<>(contributorService.getOneContributor(id).get(), HttpStatus.OK);
	}

}
