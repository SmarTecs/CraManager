
package com.cramanager.application.contributor;

import java.io.Serializable;
import java.time.ZonedDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Contributor
 * 
 * @author snadane.niveda
 *
 */
@Entity
@Table(name = "contributor")
public class Contributor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "code", nullable = false, length = 32)
	private String code;

	@Column(name = "activated", nullable = false)
	private boolean activated;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private ZonedDateTime deactivationDate;

	@Column(name = "comments")
	private String comments;

	@Column(name = "firstName", nullable = false)
	private String firstName;

	@Column(name = "lastName", nullable = false)
	private String lastName;

	private boolean isExternContributor;

	private String commentsForExternContributor;

	/**
	 * Empty Constructor
	 */
	public Contributor() {
		// Empty constructor
	}

	/**
	 * Contributor constructor
	 * 
	 * @param id
	 * @param code
	 * @param activated
	 * @param deactivationDate
	 * @param comments
	 * @param firstName
	 * @param lastName
	 * @param isExternContributor
	 * @param commentsForExternContributor
	 */
	public Contributor(int id, String code, boolean activated, ZonedDateTime deactivationDate, String comments,
			String firstName, String lastName, boolean isExternContributor, String commentsForExternContributor) {
		super();
		this.id = id;
		this.code = code;
		this.activated = activated;
		this.deactivationDate = deactivationDate;
		this.comments = comments;
		this.firstName = firstName;
		this.lastName = lastName;
		this.isExternContributor = isExternContributor;
		this.commentsForExternContributor = commentsForExternContributor;
	}

	/**
	 * Id of the contributor
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Set the id
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Get the code
	 * 
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Set the code
	 * 
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Tests if is activated
	 * 
	 * @return a boolean
	 */
	public boolean isActivated() {
		return activated;
	}

	/**
	 * Set the activation status
	 * 
	 * @param activated
	 */
	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	/**
	 * Get the activation Date
	 * 
	 * @return the date
	 */
	public ZonedDateTime getDeactivationDate() {
		return deactivationDate;
	}

	/**
	 * Set the activation date
	 * 
	 * @param deactivationDate
	 */
	public void setDeactivationDate(ZonedDateTime deactivationDate) {
		this.deactivationDate = deactivationDate;
	}

	/**
	 * Get deactivation comments
	 * 
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Set deactivation comments
	 * 
	 * @param comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Get the first name
	 * 
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Set the first name
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Get the last name
	 * 
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Set the last name
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * True if is external constructor
	 * 
	 * @return
	 */
	public boolean isExternContributor() {
		return isExternContributor;
	}

	/**
	 * Set the status of external contributor
	 * 
	 * @param isExternContributor
	 */
	public void setExternContributor(boolean isExternContributor) {
		this.isExternContributor = isExternContributor;
	}

	/**
	 * Get comments if external contributor
	 * 
	 * @return
	 */
	public String getCommentsForExternContributor() {
		return commentsForExternContributor;
	}

	/**
	 * Set comments if external contributor
	 * 
	 * @param commentsForExternContributor
	 */
	public void setCommentsForExternContributor(String commentsForExternContributor) {
		this.commentsForExternContributor = commentsForExternContributor;
	}

	/**
	 * Get the serial version UID
	 * 
	 * @return
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Auto generated EQUALS
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contributor other = (Contributor) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
