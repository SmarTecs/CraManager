package com.cramanager.application.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class UserController {
	

	enum UserSearchParametersFilters {
		LOGIN,
		EMAIL,
		LASTNAME,
		FIRSTNAME;
	}

	
	@Autowired
	private UserService userService;
	
	/**
	 * Méthode de routage pour l'url POST "/users" qui permet la création d'un utilisateur.
	 * @param input le User généré à partir du body de la requête
	 * @return HTTP OK si tout s'est bien passé, HTTP BAD_REQUEST sinon
	 */
	@RequestMapping(value = "/users", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<String> createUser(@RequestBody User input) {
		
		
		if(UserChecking.check(input) && userService.createUser(input)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	/**
	 * Méthode de routage pour l'url GET "/users" qui retourne la liste de tous les utilisateurs.
	 * @param filter est le filtre du champ à filtrer (c'est un UserController.UserSearchParametersFilters)
	 * @param value la valeur sur laquelle filtrer
	 * @return un Iterable<User> contenant tous les User.
	 */
	@RequestMapping(value = "/users", method = RequestMethod.GET, headers = "Accept=application/json")
	public Iterable<User> getUsers(@RequestHeader UserSearchParametersFilters filter, @RequestHeader String value) {
		return userService.getAllUsers(filter, value);
	}
	
	/**
	 * Méthode de routage pour l'url GET "/users/{login}" qui retourne un utilisateur.
	 * @param login le login de l'utilisateur
	 * @return l'Utilisateur s'il existe, un message d'erreur sinon
	 */
	@RequestMapping(value = "/users/{login}", method = RequestMethod.GET)
	public ResponseEntity<User> getUserByLogin(@PathVariable String login) {
		return new ResponseEntity<>(userService.findUserByLogin(login).get(), HttpStatus.OK);
	}
}