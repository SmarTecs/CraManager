package com.cramanager.application.user;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Cette classe représente un utilisateur de l'application. Son mapping en base de données est la table appusers.
 * @author Baptiste Blanchard
 *
 */
@Entity
@Table(name="appusers")
public class User implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Enum représentant le rôle de l'utilisateur.
	 * @author Baptiste Blanchard
	 *
	 */
	enum UserRole {
		DIRECTOR,
		ADMINISTRATOR,
		PROJECT_MANAGER,
		CLIENT_MANAGER;
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name= "activated")
	private boolean isActivated;
	
	@Column(name= "login", length=32, unique=true)
	private String login;
	
	@Column(name= "pwd")
	private String password;
	
	@Column(name= "email")
	private String email;
	
	@Column(name= "firstname")
	private String firstName;
	
	@Column(name= "lastname")
	private String lastName;
	
	@Column(name= "deactivationdate")
	private Date deactivationDate;
	
	@Column(name= "role")
	private UserRole role;
	
	
	public User() {
		
	}
	
	/**
	 * Constructeur manuel d'un User.
	 * @param login le login de l'utilisateur
	 * @param password le mot de passe de l'utilisateur
	 * @param deactivationDate la date de désactivation
	 * @param role le rôle de l'utilisateur
	 * @param mail l'adresse mail de l'utilisateur
	 * @param firstName le prénom de l'utilisateur
	 * @param lastName le nom de l'utilisateur
	 * @param isActivated est-ce que le compte utilisateur est activé
	 */
	public User(String login, String password, Date deactivationDate, UserRole role, String mail, String firstName, String lastName, boolean isActivated) {
		this.login = Objects.requireNonNull(login);
		this.password = Objects.requireNonNull(password);
		this.firstName = Objects.requireNonNull(firstName);
		this.lastName = Objects.requireNonNull(lastName);
		this.email = Objects.requireNonNull(mail);
		this.deactivationDate = Objects.requireNonNull(deactivationDate);
		this.role = Objects.requireNonNull(role);
		this.isActivated = isActivated;
	}
	
	/**
	 * 
	 * @return
	 */
	public UserRole getRole() {
		return role;
	}
	
	/**
	 * 
	 * @param role
	 */
	public void setRole(UserRole role) {
		this.role = role;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getIsActivated() {
		return isActivated;
	}
	
	/**
	 * 
	 * @param isActivated
	 */
	public void setIsActivated(boolean isActivated)
	{
		this.isActivated = isActivated;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getLogin() {
		return login;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getDeactivationDate() {
		return deactivationDate;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getEmail() {
		return email;
	}
	
	/**
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * 
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * 
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * 
	 * @param deactivationDate
	 */
	public void setDeactivationDate(Date deactivationDate) {
		this.deactivationDate = deactivationDate;
	}
	
}
