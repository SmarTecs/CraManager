package com.cramanager.application.user;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository persistant les opérations effectuées sur les User.
 * @author Baptiste Blanchard
 *
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long>{
	public Optional<User> findFirstByLogin(String login);
	
	public Iterable<User> findAllByEmail(String email);
	
	public Iterable<User> findAllByFirstName(String firstName);
	
	public Iterable<User> findAllByLastName(String lastName);
	
	public Iterable<User> findAllByLogin(String login);
}