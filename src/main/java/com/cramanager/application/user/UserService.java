package com.cramanager.application.user;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Service s'occupant des opérations logiques sur les utilisateurs. utilise
 * UserRepository pour la persistence des opérations.
 * 
 * @author Baptiste Blanchard
 *
 */
@Component
public class UserService {

	@Autowired
	UserRepository userRepository;

	public UserService() {

	}

	/**
	 * Tente de crée un nouvel utilisateur. Le User donné est considéré comme validé
	 * en lui-même en amont.
	 * 
	 * @param newUser
	 *            le nouvel utilisateur
	 * @return true si l'utilisateur a été ajouté, false sinon (déjà existant par
	 *         exemple)
	 */
	public boolean createUser(User newUser) {
		return userRepository.save(newUser) != null;
	}

	/**
	 * Retourne la liste de tous les utilisateurs
	 * @return la liste de tous les utilisateurs
	 */
	public Iterable<User> getAllUsers(UserController.UserSearchParametersFilters filter, String value) {
		switch(filter) {
		case EMAIL:
			return userRepository.findAllByEmail(value);
		case FIRSTNAME:
			return userRepository.findAllByFirstName(value);
		case LASTNAME:
			return userRepository.findAllByLastName(value);
		case LOGIN:
			return userRepository.findAllByLogin(value);
		}
		return userRepository.findAll();
	}

	/**
	 * Retourn un utilisateur par son login
	 * 
	 * @param login
	 *            le login de l'utilisateur
	 * @return un Optional de User pouvant contenir l'utilisateur.
	 */
	public Optional<User> findUserByLogin(String login) {
		return userRepository.findFirstByLogin(login);
	}
}
