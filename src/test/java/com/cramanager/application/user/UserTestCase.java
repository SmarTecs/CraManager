package com.cramanager.application.user;


import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;


public class UserTestCase {

	@Test(expected=NullPointerException.class)
	public void testUserLoginNotNull() {
		new User(null, "test", new Date(), User.UserRole.ADMINISTRATOR, "", "", "", true);
	}
	
	@Test(expected=NullPointerException.class)
	public void testUserPasswordNotNull() {
		new User("login", null, new Date(), User.UserRole.ADMINISTRATOR, "", "", "", true);
	}
	
	@Test(expected=NullPointerException.class)
	public void testUserDateNotNull() {
		new User("login", "test", null, User.UserRole.ADMINISTRATOR, "", "", "", true);
	}
	
	@Test(expected=NullPointerException.class)
	public void testUserRoleNotNull() {
		new User("login", "test", new Date(), null, "", "", "", true);
	}
	
	@Test(expected=NullPointerException.class)
	public void testUserEmailNotNull() {
		new User("login", "test", new Date(), User.UserRole.ADMINISTRATOR, null, "", "", true);
	}
	
	@Test(expected=NullPointerException.class)
	public void testUserFirstNameNotNull() {
		new User("login", "test", new Date(), null, "", null, "", true);
	}

	@Test(expected=NullPointerException.class)
	public void testUserLastNameNotNull() {
		new User("login", "test", new Date(), User.UserRole.ADMINISTRATOR, "", "", null, true);
	}

	@Test
	public void testUserDefaultConstructorPresent() {
		new User();
	}
	
	@Test
	public void testUserValidConstructor() {
		new User("login", "test", new Date(), User.UserRole.ADMINISTRATOR, "", "", "", true);
	}
	
	@Test
	public void testUserGetters() {
		User user = new User("login", "test", new Date(1000), User.UserRole.ADMINISTRATOR, "", "", "", true);
		assertEquals(user.getLogin(), "login");
		assertEquals(user.getPassword(), "test");
		assertEquals(user.getRole(), User.UserRole.ADMINISTRATOR);
		assertEquals(user.getLastName(), "");
		assertEquals(user.getFirstName(), "");
		assertEquals(user.getIsActivated(), true);
		assertEquals(user.getDeactivationDate(), new Date(1000));
	}
	
	@Test
	public void testUserSetters() {
		User user = new User("login", "test", new Date(1000), User.UserRole.ADMINISTRATOR, "", "", "", true);
		user.setRole(User.UserRole.DIRECTOR);
		assertEquals(user.getRole(), User.UserRole.DIRECTOR);
		
		user.setPassword("test2");
		assertEquals(user.getPassword(), "test2");
		
		user.setFirstName("test");
		assertEquals(user.getFirstName(), "test");
		
		user.setLastName("test");
		assertEquals(user.getLastName(), "test");
		
		user.setIsActivated(false);
		assertEquals(user.getIsActivated(), false);
		
		user.setDeactivationDate(new Date(2000));
		assertEquals(user.getDeactivationDate(), new Date(2000));
		
		user.setEmail("mail@gmail.com");
		assertEquals(user.getEmail(), "mail@gmail.com");
	}
	
}
