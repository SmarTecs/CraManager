package com.cramanager.application.contributor;

import static org.junit.Assert.assertEquals;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.junit.Test;

public class ContributorTestCase {

	@Test
	public void testValidContributor() {
		new Contributor(4, "123456", true, ZonedDateTime.now(), "", "Sarah", "Dupond", false, "");
	}

	@Test
	public void testForGetters() {
		Contributor c = new Contributor(4, "123456", false, ZonedDateTime.now(), "", "Sarah", "Dupond", false, "");
		assertEquals(4, c.getId());
		assertEquals("123456", c.getCode());
		assertEquals("Dupond", c.getLastName());
		assertEquals("Sarah", c.getFirstName());
	}

	@Test
	public void testForSetters() {
		Contributor c = new Contributor();

		c.setId(22);
		c.setCode("123456");
		c.setActivated(false);
		c.setFirstName("Sarah");
		c.setLastName("Dupond");
		c.setDeactivationDate(ZonedDateTime.of(2018, 01, 05, 11, 11, 11, 11, ZoneId.systemDefault()));
		c.setComments("long holidays");

		assertEquals(22, c.getId());
		assertEquals("123456", c.getCode());
		assertEquals(false, c.isActivated());
		assertEquals("Dupond", c.getLastName());
		assertEquals("Sarah", c.getFirstName());
		assertEquals(ZonedDateTime.of(2018, 01, 05, 11, 11, 11, 11, ZoneId.systemDefault()), c.getDeactivationDate());
		assertEquals("long holidays", c.getComments());
	}

	@Test
	public void testForEquals() {
		Contributor c = new Contributor(4, "123456", false, ZonedDateTime.now(), "", "Sarah", "Dupond", false, "");
		Contributor c1 = new Contributor(4, "1", false, ZonedDateTime.now(), "", "Melvyn", "Durand", false, "");
		assertEquals(c1, c);
	}

}
