package com.cramanager.application.project;

import static org.junit.Assert.assertEquals;
import org.junit.Test;


import java.util.Date;

public class ProjectTestCase {
    @Test(expected = NullPointerException.class)
    public void testProjectCodeNotNull() {
        new Project(null, 1, 1, "name", new Date(1000), new Date(1100), false, "comments", new Date(1200), "tags");
    }

    @Test(expected = NullPointerException.class)
    public void testProjectUserIdNotNull() {
        new Project("code", null, 1, "name", new Date(1000), new Date(1100), false, "comments", new Date(1200), "tags");
    }

    @Test(expected = NullPointerException.class)
    public void testProjectClientIdNotNull() {
        new Project("code", 1, null, "name", new Date(1000), new Date(1100), false, "comments", new Date(1200), "tags");
    }

    @Test(expected = NullPointerException.class)
    public void testProjectNameNotNull() {
        new Project("code", 1, 1, null, new Date(1000), new Date(1100), false, "comments", new Date(1200), "tags");
    }

    @Test(expected = NullPointerException.class)
    public void testProjectStartDateNotNull() {
        new Project("code", 1, 1, "name", null, new Date(1100), false, "comments", new Date(1200), "tags");
    }

    @Test(expected = NullPointerException.class)
    public void testProjectEndDateNotNull() {
        new Project("code", 1, 1, "name", new Date(1000), null, false, "comments", new Date(1200), "tags");
    }

    @Test(expected = NullPointerException.class)
    public void testProjectCommentsNotNull() {
        new Project("code", 1, 1, "name", new Date(1000), new Date(1100), false, null, new Date(1200), "tags");
    }

    @Test(expected = NullPointerException.class)
    public void testProjectAccountingEndDateNotNull() {
        new Project("code", 1, 1, "name", new Date(1000), new Date(1100), false, "comments", null, "tags");
    }

    @Test(expected = NullPointerException.class)
    public void testProjectTagsNotNull() {
        new Project("code", 1, 1, "name", new Date(1000), new Date(1100), false, "comments", new Date(1200), null);
    }

    @Test
    public void testProjectDefaultConstructorPresent() {
        new Project();
    }

    @Test
    public void testProjectValidConstructor() {
        new Project("code", 1, 1, "name", new Date(1000), new Date(1100), false, "comments", new Date(1200), "tags");
    }

    @Test
    public void testUserGetters() {
        Project project = new Project("code", 1, 1, "name", new Date(1000), new Date(1100), false, "comments",
                new Date(1200), "tags");
        assertEquals(project.getCode(), "code");
        assertEquals(project.getUser_id(), 1);
        assertEquals(project.getClient_id(), 1);
        assertEquals(project.getName(), "name");
        assertEquals(project.getStartDate(), new Date(1000));
        assertEquals(project.getEndDate(), new Date(1100));
        assertEquals(project.getAccountingEndDate(), new Date(1200));
        assertEquals(project.isCanceled(), false);
        assertEquals(project.isShowInTeam(), false);
        assertEquals(project.getComments(), "comments");
        assertEquals(project.getTags(), "tags");
    }

    @Test
    public void testUserSetters() {
        Project project = new Project("code", 1, 1, "name", new Date(1000), new Date(1100), false, "comments",
                new Date(1200), "tags");
        project.setCode("newCode");
        assertEquals(project.getCode(), "newCode");

        project.setUser_id(2);
        assertEquals(project.getUser_id(), 2);

        project.setClient_id(2);
        assertEquals(project.getClient_id(), 2);

        project.setName("newName");
        assertEquals(project.getName(), "newName");

        project.setStartDate(new Date(2000));
        assertEquals(project.getStartDate(), new Date(2000));

        project.setEndDate(new Date(2100));
        assertEquals(project.getEndDate(), new Date(2100));

        project.setShowInTeam(true);
        assertEquals(project.isShowInTeam(), true);

        project.setCanceled(true);
        assertEquals(project.isCanceled(), true);

        project.setComments("newComments");
        assertEquals(project.getComments(), "newComments");

        project.setAccountingEndDate(new Date(2200));
        assertEquals(project.getAccountingEndDate(), new Date(2200));

    }

}