package com.cramanager.application.task;

import java.sql.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskTest {
	@Test
	public void getIdTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals(0, task.getId());
	}

	@Test
	public void getCodeTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals("t0", task.getCode());
	}

	@Test
	public void getProjectIdTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals(0, task.getProjectId());
	}

	@Test
	public void getNameTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals("name", task.getName());
	}

	@Test
	public void getStartDateTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals(Date.valueOf("2018-02-06"), task.getStartDate());
	}

	@Test
	public void getEndDateTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals(Date.valueOf("2018-02-07"), task.getEndDate());
	}

	@Test
	public void getActivatedTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals(true, task.getActivated());
	}

	@Test
	public void setIdTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setId(1);
		Assert.assertEquals(1, task.getId());
	}

	@Test
	public void setCodeTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setCode("t1");
		Assert.assertEquals("t1", task.getCode());
	}

	@Test
	public void setProjectIdTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setProjectId(1);
		Assert.assertEquals(1, task.getProjectId());
	}

	@Test
	public void setNameTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setName("n");
		Assert.assertEquals("n", task.getName());
	}

	@Test
	public void setStartDateTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setStart(Date.valueOf("2018-02-07"));
		Assert.assertEquals(Date.valueOf("2018-02-07"), task.getStartDate());
	}

	@Test(expected=IllegalArgumentException.class)
	public void setStartDateAfterEndDateTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setStart(Date.valueOf("2018-02-08"));
	}

	@Test
	public void setEndDateTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setEnd(Date.valueOf("2018-02-08"));
		Assert.assertEquals(Date.valueOf("2018-02-08"), task.getEndDate());
	}

	@Test(expected=IllegalArgumentException.class)
	public void setEndDateBeforeStartDateTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setStart(Date.valueOf("2018-02-08"));
	}

	@Test
	public void setActivatedTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		task.setActivated(false);
		Assert.assertEquals(false, task.getActivated());
	}
	
	@Test
	public void isEndBeforeStartTest() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals(false, task.isEndBeforeStart());
	}
}
