package com.cramanager.application.task;

import java.sql.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskCheckingTest {
	@Test
	public void check() {
		Task task = new Task(0, "t0", 0, "name", Date.valueOf("2018-02-06"), Date.valueOf("2018-02-07"), true);
		Assert.assertEquals(true, TaskChecking.check(task));
	}
}
