FROM tomcat
MAINTAINER SmarTecs

COPY ./target/CRAManager-0.0.1.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run", "-Dport.http=8888"]