
/* eslint-disable */


/**
 GlobalStore est une classe définissant un objet commun à toutes les instances Vue, servant d'espace de
 stockage pour des choses comme le profil(session). Il ne remplace pas la communication parent-enfant entre
 composants mais représente plutôt une communication transversale dans toute l'application.
*/
const GlobalStore = class {
	
	constructor() {
		this.profile = null;
	}


}


export default GlobalStore
