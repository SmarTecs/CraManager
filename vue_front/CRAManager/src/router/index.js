import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home'
import Login from '@/components/Login'
import CreateUser from '@/components/CreateUser'

Vue.use(Router)

/* eslint-disable */

var router = new Router({
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home,
			meta: { requiresAuth: true }
		},
		{
			path: '/login',
			name: 'login',
			component: Login
		},
		{
			path: '/users/create',
			name: 'CreateUser',
			component: CreateUser,
			meta: { requiresAuth: true }
		}
	]
})


// Navigation Guard against unlogged user
router.beforeEach((to, from, next) => {
	if(to.matched.some(record => record.meta.requiresAuth === true) && Vue.prototype.$globalStore.profile === null) {
		next({ path: '/login'});
	}
	else {
		next();
	}
});

export default router