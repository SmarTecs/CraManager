// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'

import GlobalStore from './classes/GlobalStore'

Vue.use(Vuetify, {
	theme: {
		primary: colors.purple.accent4,
		secondary: colors.shades.black,
		accent: colors.shades.black,
		error: colors.red.accent3,
		info: '#2196F3',
		success: '#4CAF50',
		warning: '#FFC107'
  }})

Vue.config.productionTip = false

Vue.prototype.$globalStore = new GlobalStore()
Vue.mixin({
	methods: {
		isLoggedIn() {
			return Vue.prototype.$globalStore.profile !== null;
		},

		logOut() {
			Vue.prototype.$globalStore.profile = null;
			this.$router.go('/login');
		}
	}
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
